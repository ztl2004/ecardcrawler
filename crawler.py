'''
Author: Tianlei Zhang@MSRA WSM group
Alias: V-tiz
Email: ztl08@mails.tsinghua.edu.cn

PreRequest:
	Python 2.X: 2.7 recommanded

Usage:
	0.bjsuperpass website provides only the nearest 20 trans, so run this script like once a week, or more frquent...

	1.Enter the crawler folder;
	2.put your ecard_id into a file named card_ids.txt, one per line;
	3.just type:
		python crawler.py
		in your cmd(Win) or Terminal(Unix)
Feature:
	1.Extract bus ecard consuming information from the bjsuperpass website using ecard_ids;
	2.Store them in txt files named after ecard_id;
	3.Ability of restart at which ecard_id it stopped;
	
Functions:
	1.ConsumeItem: the infor class, it's readable by itself.
	2.EcardCrawler: parse using regex and store

TODO:
	1.EcardCrawler run() is a little bit complex, may be change into a decorator style...
		 
'''

import urllib, urllib2
import re
import time
import os 

class ConsumeItem:

	def __init__(self,string):
		self.card_id = ''		
		segment = string.split(',')
		
		self.consume_time = segment[0].replace(" ",",")[:-3] 
		self.consume_type = segment[1] 
		self.value = segment[2] 
		self.remain = segment[3] 
		self.company = segment[4].strip() 
		self.comment = segment[5].strip() 

	def toString(self):
		return self.card_id+','+self.consume_time+','+self.consume_type+','+self.value+','+self.remain+','+self.company+','+self.comment+'\n'


class EcardCrawler:


	def __init__(self,card_file,exist_file):
		self.card_file = card_file
		self.exist_file = exist_file
		self.card_ids = []
		self.url = "http://www.bjsuperpass.com/pagecontrol.do?object=ecard&action=query&card_id="
		
		tableString = "<TABLE cellSpacing=1(.*?)</table>" 
		consumeString = '<tr class="tr.*?</tr>'
		segmentString = '<td.*?>(.*?)</td>'
		self.tables = re.compile(tableString,re.DOTALL)
		self.consume = re.compile(consumeString,re.DOTALL)
		self.segment = re.compile(segmentString,re.DOTALL)

		try:
			self.full_card_ids = open(self.card_file,'r').readlines()
			self.card_ids = []
			try:
				self.exist_ids = open(self.exist_file,'r').readlines()
				for each in self.full_card_ids:
					if each not in self.exist_ids:
						self.card_ids.append(each)
			except:
				self.card_ids = self.full_card_ids

		except:
			print 'no card_file found...'

	def parse(self,content):
		print 'parsing...'
		tables = self.tables.findall(content)
		acturalTable = tables[2]
		consumes = self.consume.findall(acturalTable)
		
		consumeList = []
		for consume in consumes:
			segments = self.segment.findall(consume)
			consumeString = ','.join(segments)
			consumeItem = ConsumeItem(consumeString)
			consumeList.append(consumeItem)
		return consumeList

	def run(self):
		length = len(self.card_ids)
		counter = 0

		for card in self.card_ids:
			counter += 1
			print 'progress: %d out of %d'%(counter,length) 
			card = card[:17]
			print card
			record_file = open(card,'w')
			try:
				content = urllib2.urlopen(self.url+card).read()
				consumeList = self.parse(content)
				print 'writing into file...'
				for consumeItem in consumeList:
					consumeItem.card_id = card
					record_file.write(consumeItem.toString())
				record_file.close()
				
				f = open('exist_ids.txt','w')
				f.write(card+'\n')
				f.close()	

			except Exception, e:
				print e
				#print content
				print 'something is wrong...' 
			print 'damn website requires a sleep of 30 secs...'
			time.sleep(30)
		os.remove('exist_ids.txt')
		


if __name__ == "__main__":
	crawler = EcardCrawler('card_ids.txt','exist_ids.txt')
	crawler.run()